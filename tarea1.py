#RobertArevalo
#FranciscoSolis

import random

class Animal(object):
    def __init__(self, energia, nombre, edad):
        self.energia = energia
        self.nombre = nombre
        self.edad = edad

    def getNombre(self):
        return self.nombre

    def getEdad(self):
        return self.edad

    def caminar(self):
        self.energia=self.energia-200
        return self.energia
    
    def comer(self):
        self.energia=self.energia+300
        return self.energia

    def enfermar(self):
        self.energia=self.energia-(random.randrange(1000))
        return self.energia
  

class Perro(Animal):
    def __init__(self, energia, nombre, edad):
        Animal.__init__(self, energia, nombre, edad)
        self.energia = energia
        self.nombre = nombre
        self.edad = edad
            
    def ladrar(self):
        self.energia = self.energia-100
        return self.energia
    
    def correr(self):
        self.energia = self.energia-200
        return self.energia

    def __add__(self, objeto):
        suma_nombre = self.nombre + objeto.nombre
        
        if (self.edad > objeto.edad):
            edad_mayor = objeto.edad
        else:
            edad_mayor = self.edad

        suma_energia = self.energia + objeto.energia

        suma_total = Perro(suma_energia, suma_nombre, edad_mayor)

        return suma_total
        

if __name__ == '__main__':
    
    animal1 = Perro(1000, 'Puppy',2)
    animal2 = Perro(1000, 'Doki',5)

    print ("El animal se llama: ", animal1.getNombre())
    print ("Tiene: ", animal1.getEdad(),"años de edad.")
    print ("Al caminar el animal pierde 200 de energia, asi que su energia es: ", animal1.caminar())
    print ("Al comer el animal recupera 300 de energia, asi que su energia es: ", animal1.comer())
    print ("Al ladrar el animal pierde 100 de energia, asi que su energia es: ", animal1.ladrar())
    print ("Al enfermar el animal pierde energia aleatoria, asi que su energia es: ", animal1.enfermar())

    print ("\n\n")

    print ("El animal se llama: ", animal2.getNombre())
    print ("Tiene: ", animal2.getEdad(),"años de edad.")
    print ("Al caminar el animal pierde 200 de energia, asi que su energia es: ", animal2.caminar())
    print ("Al comer el animal recupera 300 de energia, asi que su energia es: ", animal2.comer())
    print ("Al ladrar el animal pierde 100 de energia, asi que su energia es: ", animal2.ladrar())
    print ("Al enfermar el animal pierde energia aleatoria, asi que su energia es: ", animal2.enfermar())

    print ("\n\n")
    
    suma_total= animal1+animal2
    
    print ("Concatenacion de los dos nombres: ", suma_total.nombre)
    print ("La edad del perro mas joven es: ", suma_total.edad, "años de edad")
    print ("La suma de las unidades de energia es: ", suma_total.energia)
    
    print ("\n\n")



